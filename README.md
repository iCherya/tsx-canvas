# tsx-canvas

Utility for drawing a figure by points on the canvas

## How to use

Specify the required coordinates of points (each coordinate on a new line) in the textarea in the format "x y". Or upload specification .txt file.

[Min, Max] = [0, 1000]

Example:

```
100 100
100 200
200 200
250 150
200 100
```

## Building and running on localhost

First install dependencies:

```sh
npm install
```

To run in hot module reloading mode:

```sh
npm start
```

To create a production build:

```sh
npm run build
```
