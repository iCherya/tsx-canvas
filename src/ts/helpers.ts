interface Point {
  x: number;
  y: number;
}

const getStructuredData = (input: string): Point[] =>
  input
    .trim()
    .split('\n')
    .map((line) => line.split(' ').map(Number))
    .map(([x, y]) => ({ x, y }));

const calculateDistance = (point1: Point, point2: Point): number => {
  const { x: x1, y: y1 } = point1;
  const { x: x2, y: y2 } = point2;
  const xDist = x2 - x1;
  const yDist = y2 - y1;

  return Math.sqrt(Math.pow(xDist, 2) + Math.pow(yDist, 2));
};

const readFileContent = (file: File): Promise<unknown> => {
  const reader = new FileReader();

  return new Promise((resolve, reject) => {
    reader.onload = (event) => resolve(event.target?.result);
    reader.onerror = (error) => reject(error);
    reader.readAsText(file);
  });
};

export { getStructuredData, calculateDistance, readFileContent, Point };
