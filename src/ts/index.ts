import {
  calculateDistance,
  readFileContent,
  getStructuredData,
  Point
} from './helpers';

const canvas = <HTMLCanvasElement>document.getElementById('canvas');
const ctx = <CanvasRenderingContext2D>canvas.getContext('2d');

const inputFileElement = <HTMLElement>document.getElementById('input-file');
const textareaElement = <HTMLInputElement>document.getElementById('textarea');
const perimeterOutput = <HTMLInputElement>document.getElementById('perimeter');
const areaOutput = <HTMLInputElement>document.getElementById('area');

const drawOnCanvas = (input: string): void => {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  if (!input) return;
  const data = getStructuredData(input);

  ctx.beginPath();
  data.forEach(({ x, y }) => {
    ctx.lineTo(x, y);
  });
  ctx.fill();
  ctx.closePath();

  perimeterOutput.textContent = calculatePerimeter(data).toFixed(2);
  areaOutput.textContent = calculateArea().toFixed(2);
};

const calculatePerimeter = (data: Point[]): number =>
  [...data, data[0]].reduce((acc, curr, idx, arr) => {
    if (!arr[idx + 1]) return acc;

    return acc + calculateDistance(curr, arr[idx + 1]);
  }, 0) || 0;

const calculateArea = (): number => {
  const width = canvas.width;
  const height = canvas.height;

  const data = ctx.getImageData(0, 0, width, height).data;

  const totalCanvasPixels = data.length / 4;
  const transparentPixels = data
    .slice(3)
    .filter((_, idx) => idx % 4 === 0)
    .reduce((acc, curr) => acc + (curr ? 0 : 1), 0);

  return totalCanvasPixels - transparentPixels;
};

inputFileElement.addEventListener('change', (event: Event) => {
  const input = <HTMLInputElement>event.target;
  const files = <FileList>input.files;

  if ('files' in input && files.length > 0) {
    const file = files[0];

    readFileContent(file)
      .then((fileContent: any) => {
        textareaElement.innerHTML = fileContent;
        drawOnCanvas(fileContent);
      })

      .catch((error: Error) => console.log(error));
  }
});

textareaElement.addEventListener('input', () => {
  const data = textareaElement.value;
  drawOnCanvas(data);
});
